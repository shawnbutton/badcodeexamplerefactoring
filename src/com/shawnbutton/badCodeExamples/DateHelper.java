package com.shawnbutton.badCodeExamples;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class DateHelper {

    private static final int FOURTEEN_DAYS = 14;

    // private constructor since static utility class
    private DateHelper() {
    }

    static int getMonthNumber(Date expiryDate) {
        SimpleDateFormat monthOnlyFormat = new SimpleDateFormat("MM");
        return Integer.parseInt((monthOnlyFormat.format(expiryDate)));
    }

    static int getDayOfMonth(Date endDate) {
        SimpleDateFormat dayOnlyFormat = new SimpleDateFormat("dd");
        return Integer.parseInt((dayOnlyFormat.format(endDate)));
    }

    static Date date14DaysFrom(Date currentDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DATE, FOURTEEN_DAYS);
        return cal.getTime();
    }
}