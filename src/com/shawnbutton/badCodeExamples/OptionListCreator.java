package com.shawnbutton.badCodeExamples;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OptionListCreator {

    private static final int DAYS_IN_MONTH = 31;
    private static final int MONTHS_IN_YEAR = 12;

    public List<SelectOption> createOptionsForMonthsWithGivenMonthSelected(Date expiryDate) {

        int monthToSelect = DateHelper.getMonthNumber(expiryDate);

        return createSelectOptions(MONTHS_IN_YEAR, monthToSelect);
	}

    public List<SelectOption> createOptionsForDaysOfMonthWithDay14DaysInTheFutureSelected(Date currentDate) {

        Date dayToSelect = DateHelper.date14DaysFrom(currentDate);

        return createOptionsForDaysOfMonthWithGivenDaySelected(dayToSelect);
	}

    public List<SelectOption> createOptionsForDaysOfMonthWithGivenDaySelected(Date expiryDate) {

        int dayToSelect = DateHelper.getDayOfMonth(expiryDate);

        return createSelectOptions(DAYS_IN_MONTH, dayToSelect);
    }

    private void selectOptionNumber(List<SelectOption> optionList, int optionToSelect) {
        // must subtract one because the list is zero-based
        optionList.get(optionToSelect - 1).setSelected(true);
    }

    private List<SelectOption> createSelectOptions(int numberOfOptions, int optionToSelect) {
        List<SelectOption> selectOptions = createSelectOption(numberOfOptions);

        selectOptionNumber(selectOptions, optionToSelect);

        return selectOptions;
    }

    private List<SelectOption> createSelectOption(int numberOfOptions) {
        List<SelectOption> selectOptions = new ArrayList<SelectOption>();
        for (int optionOn = 1; optionOn <= numberOfOptions; optionOn++) {
            SelectOption option = createOptionAndSetLabelAndValueTo(optionOn);
            selectOptions.add(option);
        }
        return selectOptions;
    }

    private SelectOption createOptionAndSetLabelAndValueTo(int optionValue) {
        SelectOption option = new SelectOption();

        final String dayValue = String.valueOf(optionValue);
        option.setLabel(dayValue);
        option.setValue(dayValue);

        option.setSelected(false);

        return option;
    }

}
