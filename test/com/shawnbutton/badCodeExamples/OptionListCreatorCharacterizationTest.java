package com.shawnbutton.badCodeExamples;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;


/**
 * Characterization tests which validate current behaviour of OptionListCreator
 *
 * These tests can be used to provide test coverage until OptionListCreator is refactored,
 * at which point they should likely be replace with better formatted tests oriented along
 * functionalityv lines.
 */
public class OptionListCreatorCharacterizationTest {

    private static final int YEAR_2012 = 112;
    private static final int MONTH_SEPTEMBER = 8;
    private static final int DAY_OF_MONTH_11 = 10;

    private Date inputDate;
    private OptionListCreator optionListCreator;

    @Before
    public void setUp() {
        inputDate = new Date(YEAR_2012, MONTH_SEPTEMBER, DAY_OF_MONTH_11);
        optionListCreator = new OptionListCreator();
    }

    @Test
    public void testCheckEndMonthList() throws Exception {

        final List<SelectOption> returnedOptions = optionListCreator.createOptionsForMonthsWithGivenMonthSelected(inputDate);

        assertThat("list contains 12 items (one per month)", returnedOptions.size(), equalTo(12));
        for (int listItemOn = 0; listItemOn < returnedOptions.size(); listItemOn++) {
            SelectOption optionBeingVerified = returnedOptions.get(listItemOn);
            assertThat("list item has label matching list number plus one", optionBeingVerified.getLabel(), equalTo(String.valueOf(listItemOn+1)));
            assertThat("list item has value matching list number plus one", optionBeingVerified.getValue(), equalTo(String.valueOf(listItemOn+1)));

            if (listItemOn == MONTH_SEPTEMBER) {
                assertTrue("list item is true on selected month", optionBeingVerified.isSelected());
            } else {
                assertFalse("list item is false on non-selected month", optionBeingVerified.isSelected());
            }
        }

    }

    @Test
    public void testCreateEndDaysList() throws Exception {

        final List<SelectOption> returnedOptions = optionListCreator.createOptionsForDaysOfMonthWithDay14DaysInTheFutureSelected(inputDate);

        assertThat("list contains 31 items (one per day in month)", returnedOptions.size(), equalTo(31));
        for (int listItemOn = 0; listItemOn < returnedOptions.size(); listItemOn++) {
            SelectOption optionBeingVerified = returnedOptions.get(listItemOn);
            assertThat("list item has label matching list number plus one", optionBeingVerified.getLabel(), equalTo(String.valueOf(listItemOn+1)));
            assertThat("list item has value matching list number plus one", optionBeingVerified.getValue(), equalTo(String.valueOf(listItemOn+1)));

            if (listItemOn == DAY_OF_MONTH_11 + 13) {
                assertTrue("list item is true on input day of month plus 13", optionBeingVerified.isSelected());
            } else {
                assertFalse("list item is false on every day of month except input plus 13", optionBeingVerified.isSelected());
            }
        }

    }

    @Test
    public void testCheckEndDaysList() throws Exception {

        final List<SelectOption> returnedOptions = optionListCreator.createOptionsForDaysOfMonthWithGivenDaySelected(inputDate);

        assertThat("list contains 31 items (one per day in month)", returnedOptions.size(), equalTo(31));
        for (int listItemOn = 0; listItemOn < returnedOptions.size(); listItemOn++) {
            SelectOption optionBeingVerified = returnedOptions.get(listItemOn);
            assertThat("list item has label matching list number plus one", optionBeingVerified.getLabel(), equalTo(String.valueOf(listItemOn+1)));
            assertThat("list item has value matching list number plus one", optionBeingVerified.getValue(), equalTo(String.valueOf(listItemOn+1)));

            if (listItemOn == DAY_OF_MONTH_11 - 1) {
                assertTrue("list item is true on input day of month minus 1", optionBeingVerified.isSelected());
            } else {
                assertFalse("list item is false on every day of month except input minus 1", optionBeingVerified.isSelected());
            }
        }
    }

}
